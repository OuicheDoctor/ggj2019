﻿namespace Windhome.FileAPI
{
    using UniRx;
    using UniRx.Triggers;
    using UnityEngine;
    using UnityEngine.UI;

    public class Alert : MonoBehaviour
    {
        public Text alertText;

        public Image button;

        public AudioSource source;

        public void Init(string text)
        {
            alertText.text = text;
            source.Play();
        }

        private void Start()
        {
            button.OnPointerClickAsObservable()
                .Subscribe(_ =>
                {
                    Destroy(gameObject);
                })
                .AddTo(gameObject);
        }
    }
}