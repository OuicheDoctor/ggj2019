﻿using System;

namespace Windhome.FileAPI
{
    using UniRx;
    using UnityEngine;
    using UnityEngine.SceneManagement;
    using Windhome.Interfaces;

    public class FileAPIMgr : MonoBehaviour
    {
        [SerializeField]
        private GameObject _desktop;

        [SerializeField]
        private ImagePlayer _imagePlayer;

        [SerializeField]
        private MyVideoPlayer _videoPlayer;

        [SerializeField]
        private AudioPlayer _audioPlayer;

        [SerializeField]
        private TextEditor _textEditor;

        public FileReader fileReader;

        public TaskManager _taskMgr;

        public IExplorer _iexplorer;

        public Prompt _passPrompt;

        public Alert _alert;

        public MappingFiles mapping;
        
        public Windhome.Cursor _cursor;
        
        public bool switchNextManoir = false;

        private static FileAPIMgr _instance;

        private void Awake()
        {
            _instance = this;
            mapping.Init();
        }

        public static void OpenEditor(Mapping map)
        {
            TextEditor window = Instantiate(_instance._textEditor, _instance._desktop.transform);
            window.Init(map);
            window.GetComponent<Window>().SetFocusControl();
        }

        public static AudioPlayer OpenAudio(Mapping map)
        {
            AudioPlayer window = Instantiate(_instance._audioPlayer, _instance._desktop.transform);
            window.Init(map);
            window.GetComponent<Window>().SetFocusControl();

            return window;
        }

        public static ImagePlayer OpenImage(Mapping map, Vector2 fromClickPos)
        {
            ImagePlayer imagePlayer = Instantiate(_instance._imagePlayer, _instance._desktop.transform);
            Window window = imagePlayer.GetComponent<Window>();
            imagePlayer.Init(map, fromClickPos);
            window.SetFocusControl();
            
            if (map.ID == 122)
            {
                window.onClose.Subscribe(b =>
                {
                    map.Resource = FileAPIMgr.GetMapping(123).Resource;
                    _instance._cursor.ForcedPosition = fromClickPos;
                    _instance._cursor.Haunted = true;
                    IDisposable disposable = _instance._cursor.onMove.Subscribe(_ =>
                    {
                        if (Math.Abs(_.x - fromClickPos.x) < 50 && Math.Abs(_.y - fromClickPos.y) < 50)
                        {
                            _instance._cursor.Haunted = false;
                        }
                    });
                });   
            }
            
            RectTransform rectTransform = imagePlayer.GetComponent<RectTransform>();
            Sprite imageSprite = imagePlayer._image.sprite;
            rectTransform.sizeDelta = new Vector2(Math.Min(640f, imageSprite.rect.size.x), Math.Min(460f, imageSprite.rect.size.y));
            rectTransform.anchorMin = new Vector2(0.5f,0.5f);
            rectTransform.anchorMax = new Vector2(0.5f,0.5f);
            rectTransform.pivot = new Vector2(0.5f, 0.5f);
            rectTransform.localPosition = new Vector3((640-imageSprite.rect.size.x)/2, (460-imageSprite.rect.size.y)/2);

            return imagePlayer;
        }

        public static void OpenVideo(Mapping map)
        {
            MyVideoPlayer window = Instantiate(_instance._videoPlayer, _instance._desktop.transform);
            window.Init(map);
            window.Play();
        }

        public static FileReader OpenFolder(Mapping map)
        {
            FileReader instantiate = Instantiate(_instance.fileReader, _instance._desktop.transform);
            instantiate.LoadFiles(map);
            instantiate.GetComponent<Window>().SetFocusControl();

            return instantiate;
        }

        public static void OpenTaskManager()
        {
            var window = Instantiate(_instance._taskMgr, _instance._desktop.transform);
            window.GetComponent<Window>().SetFocusControl();

        }

        public static void OpenIE()
        {
            var window = Instantiate(_instance._iexplorer, _instance._desktop.transform);
            window.GetComponent<Window>().SetFocusControl();

        }

        public static void OpenPasswordPrompt()
        {
            var prompt = Instantiate(_instance._passPrompt, _instance._desktop.transform);
            prompt.OnSuccess
                .Subscribe(_ =>
                {
                    SceneManager.LoadScene("BSOD");
                })
                .AddTo(prompt);
            prompt.GetComponent<Window>().SetFocusControl();
        }

        public static void OpenAlert(string text)
        {
            var alert = Instantiate(_instance._alert, _instance._desktop.transform);
            alert.Init(text);
            alert.GetComponent<Window>().SetFocusControl();

        }

        public static Mapping GetMapping(int ID)
        {
            return _instance.mapping.Mapping.Find(m => m.ID == ID);
        }
    }

}