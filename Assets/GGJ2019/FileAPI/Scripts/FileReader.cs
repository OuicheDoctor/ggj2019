﻿namespace Windhome.FileAPI
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using UnityEngine;
    using UnityEngine.UI;

    public class FileReader : MonoBehaviour
    {
        public FileEntry dirEntry;
        public FileEntry fileEntry;
        public Text _windowTitle;

        public GameObject windowContent;
        public const string GGJ_FOLDER = "Resources";
        public const string C_DRIVE = "RF";

        private int fileCnt = 0;
        private List<Mapping> directoryList;
        private List<Mapping> fileList;
    
        // Start is called before the first frame update
        public void LoadFiles(Mapping map)
        {
            _windowTitle.text = map.Name;
            fileList = map.Children.Where(m => m.Type != FileEntry.FileType.Dir).ToList();
            directoryList = map.Children.Where(m => m.Type == FileEntry.FileType.Dir).ToList();

            foreach (var directoryInfo in directoryList)
            {
                addDir(directoryInfo);
            }

            foreach (var fileInfo in fileList)
            {
                if (fileInfo.Name != "ManoirWithEmmett.gif")
                {
                    addFile(fileInfo);
                }
            }
        }

        private void addDir(Mapping dirInfos)
        {
            var thatFileEntry = Instantiate(dirEntry, this.windowContent.transform);
            Text myText = thatFileEntry.GetComponent<Text>();
            myText.text = dirInfos.Name;
            thatFileEntry.mapping = dirInfos;
            thatFileEntry.ListenEvent();
            fileCnt++;
        }

        private void addFile(Mapping fileInfo)
        {
            //        Debug.Log("FileReader.Start fileInfo " + fileInfo.Name);
            var thatFileEntry = Instantiate(fileEntry, this.windowContent.transform);
            Text myText = thatFileEntry.GetComponent<Text>();
            myText.text = fileInfo.Name;
            thatFileEntry.mapping = fileInfo;
            thatFileEntry.ListenEvent();
            fileCnt++;
        }
    }
}