﻿namespace Windhome.FileAPI
{
    using System.IO;
    using TMPro;
    using UnityEngine;
    using UnityEngine.UI;

    public class TextEditor : MonoBehaviour
    {
        public Text _windowTitle;

        public TMP_InputField _textArea;

        public void Init(Mapping map)
        {
            _windowTitle.text = map.Name;
            var text = (map.Resource as TextAsset).text;
            _textArea.text = text;
        }
    }
}