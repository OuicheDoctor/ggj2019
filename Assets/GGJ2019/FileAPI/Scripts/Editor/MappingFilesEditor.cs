﻿namespace Windhome.FileAPI
{
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using UnityEditor;
    using UnityEngine;
    using static Windhome.FileAPI.FileEntry;

    [CustomEditor(typeof(MappingFiles))]
    public class MappingFilesEditor : Editor
    {
        private int _parentID = -1;
        private string _rootFolder;
        private int _nextID = 0; 
        private Regex _reg = new Regex(@"\.meta$");
        private MappingFiles _mapping;

        public override void OnInspectorGUI()
        {
            _mapping = (MappingFiles)target;

            _rootFolder = EditorGUILayout.TextField("Root Folder", _rootFolder);
            if (GUILayout.Button("Parse"))
            {
                _nextID = 0;
                ProcessFolder(_rootFolder);
            }

            base.OnInspectorGUI();
        }

        private void ProcessFolder(string path, int parentID = -1)
        {
            var dirInfo = new DirectoryInfo(path);
            var files = dirInfo.GetFiles().Where(f => !_reg.IsMatch(f.Name)).ToList();
            foreach(var f in files)
            {
                _mapping.AddMapping(_nextID, f.Name, GetFileType(f.Extension), parentID);
                _nextID++;
            }

            var directories = dirInfo.GetDirectories();
            foreach (var d in directories)
            {
                var dId = _nextID;
                _mapping.AddMapping(dId, d.Name, FileType.Dir, parentID);
                _nextID++;
                ProcessFolder(d.FullName, dId);
            }
        }

        private FileType GetFileType(string extension)
        {
            switch(extension)
            {
                case ".mp3":
                    return FileEntry.FileType.Audio;
                case ".webm":
                    return FileEntry.FileType.Video;
                case ".mp4":
                    return FileEntry.FileType.Video;
                case ".txt":
                    return FileEntry.FileType.Text;
                case ".rtf":
                    return FileEntry.FileType.Text;
                case ".jpg":
                    return FileEntry.FileType.Image;
                case ".gif":
                    return FileEntry.FileType.Image;
                case ".png":
                    return FileEntry.FileType.Image;
                default:
                    return FileType.Dir;
            }
        }
    }
}