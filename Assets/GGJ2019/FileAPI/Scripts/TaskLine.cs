﻿namespace Windhome.FileAPI
{
    using UnityEngine;
    using UnityEngine.UI;

    public class TaskLine : MonoBehaviour
    {
        public Image Background;
        public Text Text;
    }
}