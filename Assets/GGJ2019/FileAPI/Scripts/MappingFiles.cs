﻿namespace Windhome.FileAPI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using static Windhome.FileAPI.FileEntry;

    [CreateAssetMenu(fileName = "FileMapping", menuName = "Custom/FileMapping")]
    public class MappingFiles : ScriptableObject
    {
        public List<Mapping> Mapping;

        public Dictionary<int, Mapping> Index = new Dictionary<int, Mapping>();

        public void Init()
        {
            Index.Clear();
            foreach(var map in Mapping)
            {
                Index.Add(map.ID, map);
                map.Children = Mapping.Where(m => m.ParentID == map.ID).ToList();
            }
        }
        
        public void AddMapping(int ID, string Name, FileType Type, int ParentID = -1)
        {
            Mapping.Add(new FileAPI.Mapping()
            {
                ID = ID,
                Name = Name,
                Type = Type,
                ParentID = ParentID
            });
        }
    }

    [Serializable]
    public class Mapping
    {
        public int ID = 0;

        public string Name;

        public FileType Type;

        public int ParentID;

        public UnityEngine.Object Resource;

        [NonSerialized]
        public List<Mapping> Children;
    }
}