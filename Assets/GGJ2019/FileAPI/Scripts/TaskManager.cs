﻿namespace Windhome.FileAPI
{
    using System.Collections.Generic;
    using UniRx;
    using UniRx.Triggers;
    using UnityEngine;
    using UnityEngine.SceneManagement;
    using UnityEngine.UI;

    public class TaskManager : MonoBehaviour
    {
        public TaskLine line;

        public List<string> tasks = new List<string>()
        {
            "Hope.exe",
            "Unlife.exe",
            "YouLeftMeAlone.exe",
            "PleaseDontHurtMe.exe",
        };

        public Transform TaskContainer;

        public Image TaskKiller;

        private void Start()
        {
            Init();
        }

        private TaskLine currentLine;

        public void Init()
        {
            foreach(var t in tasks)
            {
                var taskLine = Instantiate(line, TaskContainer);
                taskLine.GetComponentInChildren<Text>().text = t;
                taskLine.Background.OnPointerClickAsObservable()
                    .Subscribe(_ =>
                    {
                        if (currentLine != null)
                            currentLine.Background.color = Color.white;

                        currentLine = taskLine;
                        currentLine.Background.color = Color.blue;
                    })
                    .AddTo(taskLine);
            }

            TaskKiller.OnPointerClickAsObservable()
                .Subscribe(_ =>
                {
                    if (currentLine != null)
                    {
                        if (currentLine.Text.text != "Unlife.exe")
                        {
                            Destroy(currentLine.gameObject);
                        }
                        else
                        {
                            FileAPIMgr.OpenPasswordPrompt();
                        }
                    }
                })
                .AddTo(this);
        }

    }
}