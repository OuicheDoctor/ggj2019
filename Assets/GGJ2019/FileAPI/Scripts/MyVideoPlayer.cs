﻿namespace Windhome.FileAPI
{
    using UniRx;
    using UniRx.Triggers;
    using UnityEngine;
    using UnityEngine.UI;
    using UnityEngine.Video;

    public class MyVideoPlayer : MonoBehaviour
    {
        public VideoPlayer _player;
        public Image _playButton;
        public Image _nextFrameButton;
        public Image _previousFrameButton;
        public Image _stopButton;
        public Image _progress;
        public Image _progressCursor;
        public Text _windowTitle;

        public void Play()
        {
            _player.Play();
        }

        public void Init(Mapping map)
        {
            _windowTitle.text = map.Name;
            _player.clip = map.Resource as VideoClip;
        }

        private void Start()
        {
            _playButton.OnPointerClickAsObservable()
                .Subscribe(_ => PlayButtonClicked())
                .AddTo(this);

            _stopButton.OnPointerClickAsObservable()
                .Subscribe(_ =>
                {
                    _player.Stop();
                })
                .AddTo(this);

            _nextFrameButton.OnPointerClickAsObservable()
                .Subscribe(_ =>
                {
                    _player.StepForward();
                })
                .AddTo(this);

            _previousFrameButton.OnPointerClickAsObservable()
                .Subscribe(_ =>
                {
                    _player.frame--;
                })
                .AddTo(this);

            _player.UpdateAsObservable()
                .Subscribe(_ =>
                {
                    var current = _progressCursor.transform.localPosition;
                    current.x = _player.frame / (float)_player.frameCount * ((_progress.transform as RectTransform).sizeDelta.x - 2) + 2;
                    _progressCursor.transform.localPosition = current;
                });
        }

        private void PlayButtonClicked()
        {
            if(_player.isPlaying)
            {
                if(_player.isPaused)
                {
                    _player.Play();
                }
                else
                {
                    _player.Pause();
                }
            }
            else
            {
                _player.Play();
            }
        }

    }
}