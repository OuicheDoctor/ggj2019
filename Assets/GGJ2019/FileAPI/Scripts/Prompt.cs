﻿namespace Windhome.FileAPI
{
    using System.Collections.Generic;
    using UniRx;
    using UniRx.Triggers;
    using UnityEngine;
    using UnityEngine.UI;

    public class Prompt : MonoBehaviour
    {
        public InputField Password;

        public List<string> AcceptablePassword;

        public List<string> FailedTaunt;

        public Subject<bool> OnSuccess = new Subject<bool>();

        public Image Button;

        public Image CloseButton;

        private void Start()
        {
            Button.OnPointerClickAsObservable()
                .Subscribe(_ =>
                {
                    Validate();
                })
                .AddTo(this);

            CloseButton.OnPointerClickAsObservable()
                .Subscribe(_ =>
                {
                    Destroy(gameObject);
                })
                .AddTo(this);
        }

        public void Validate()
        {
            var lowerCase = Password.text.ToLower();
            foreach(var p in AcceptablePassword)
            {
                if (lowerCase.Contains(p))
                {
                    Debug.Log("Success");
                    OnSuccess.OnNext(true);
                    return;
                }
            }

            FileAPIMgr.OpenAlert(FailedTaunt[Random.Range(0, FailedTaunt.Count)]);
            Debug.Log("Fail");
        }
    }
}