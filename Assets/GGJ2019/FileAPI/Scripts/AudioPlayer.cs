﻿using System;

namespace Windhome.FileAPI
{
    using UniRx;
    using UniRx.Triggers;
    using UnityEngine;
    using UnityEngine.UI;

    public class AudioPlayer : MonoBehaviour
    {
        public AudioSource Source;

        public Image _playButton;
        public Image _nextFrameButton;
        public Image _previousFrameButton;
        public Image _stopButton;
        public Image _progress;
        public Image _progressCursor;

        public Text _windowTitle;

        public void Init(Mapping map)
        {
            _windowTitle.text = map.Name;
            Source.clip = map.Resource as AudioClip;
            Source.Play();
        }

        private void Start()
        {
            _playButton.OnPointerClickAsObservable()
                .Subscribe(_ => PlayButtonClicked())
                .AddTo(this);

            _stopButton.OnPointerClickAsObservable()
                .Subscribe(_ =>
                {
                    Source.Stop();
                })
                .AddTo(this);

            _nextFrameButton.OnPointerClickAsObservable()
                .Subscribe(_ =>
                {
                    Source.time += 1f;
                })
                .AddTo(this);

            _previousFrameButton.OnPointerClickAsObservable()
                .Subscribe(_ =>
                {
                    Source.time -= 1f;
                })
                .AddTo(this);

            Source.UpdateAsObservable()
                .Subscribe(_ =>
                {
                    if (!Source.isPlaying)
                        return;

                    var current = _progressCursor.transform.localPosition;
                    current.x = Source.time / (float)Source.clip.length * ((_progress.transform as RectTransform).sizeDelta.x - 2) + 2;
                    _progressCursor.transform.localPosition = current;
                });
        }

        public void PlayButtonClicked()
        {
            if (Source.isPlaying)
            {
                Source.Pause();
            }
            else
            {
                Source.Play();
            }
        }
    }
}