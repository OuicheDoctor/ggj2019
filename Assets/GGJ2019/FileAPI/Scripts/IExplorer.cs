﻿namespace Windhome
{
    using System.Collections.Generic;
    using DG.Tweening;
    using UniRx;
    using UniRx.Triggers;
    using UnityEngine;
    using UnityEngine.UI;

    public class IExplorer : MonoBehaviour
    {
        public List<GameObject> Popups;

        public List<Transform> PopupsSpawn;

        private void Start()
        {
            var sequence = DOTween.Sequence();
            for (var i = 0; i < Popups.Count; i++)
            {
                var popup = Popups[i];
                var spawn = PopupsSpawn[i];
                sequence.AppendCallback(() => 
                { 
                    var pop = Instantiate(
                        popup,
                        spawn);
                    pop
                        .GetComponent<Image>()
                        .OnPointerClickAsObservable()
                        .Subscribe(_ => Destroy(pop))
                        .AddTo(pop);
                });
                sequence.AppendInterval(.5f);
            }
        }

    }
}