﻿namespace Windhome.FileAPI
{
    using System;
    using System.IO;
    using UniRx;
    using UniRx.Triggers;
    using UnityEngine;
    using UnityEngine.UI;

    public class FileEntry : MonoBehaviour
    {
        [Serializable]
        public enum FileType
        {
            Audio,
            Video,
            Text,
            Dir,
            Image
        }

        public Mapping mapping;

        public GameObject desktop;
        public GameObject fileListWindowPrefab;

        public void ListenEvent()
        {
            Text fileText = GetComponent<Text>();
            var clickStream = fileText.OnPointerClickAsObservable();
            clickStream
                .Buffer(clickStream.Throttle(TimeSpan.FromMilliseconds(250)))
                .Where(xs => xs.Count >= 2)
                .Subscribe(_ =>
                {
                    //Debug.Log("Open file : " + fileText.text);
                    //var absolutePath = FileReader.C_DRIVE;
                    //if (string.IsNullOrEmpty(this.absolutePath))
                    //    absolutePath = Path.Combine(FileReader.C_DRIVE, this.absolutePath);

                    //absolutePath = Path.Combine(absolutePath, fileText.text);
                    //absolutePath = absolutePath.Split('.')[0];

                    Vector2 lastClickPos = _[0].position;

                    switch (mapping.Type)
                    {
                        case FileType.Audio:
                            Debug.Log("FileEntry audio with path " + fileText.text);
                            FileAPIMgr.OpenAudio(mapping);
                            break;

                        case FileType.Video:
                            Debug.Log("FileEntry video with path " + fileText.text);
                            FileAPIMgr.OpenVideo(mapping);
                            break;

                        case FileType.Text:
                            Debug.Log("FileEntry text with path " + fileText.text);
                            FileAPIMgr.OpenEditor(mapping);
                            break;

                        case FileType.Dir:
                            var fileReader = FileAPIMgr.OpenFolder(mapping);
                            fileReader.transform.localPosition 
                                = new Vector3(UnityEngine.Random.Range(-20, 20), UnityEngine.Random.Range(-20, 20), 0);
                            break;
                        case FileType.Image:
                            ImagePlayer imagePlayer = FileAPIMgr.OpenImage(mapping, lastClickPos);
                            break;
                    }
                })
                .AddTo(this);
        }

        // Start is called before the first frame update
        private void Start()
        {
        }
    }
}