﻿namespace Windhome
{
    using UnityEngine;
    using UnityEngine.UI;
    using Windhome.FileAPI;
    using System;

    public class ImagePlayer : MonoBehaviour
    {
        public Image _image;
        public Text _windowTitle;

        public void Init(Mapping map, Vector2 fromClickPos)
        {
            _windowTitle.text = map.Name;
            _image.sprite = map.Resource as Sprite;
        }
    }
}
