﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UniRx;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using Windhome.FileAPI;
using Windhome.Interfaces;
using Random = UnityEngine.Random;

public class RandomSoundPlayer : MonoBehaviour
{
    [Tooltip("% de chances de se déclencher toutes les secondes")]
    public int chancesRate;

    public List<int> soundIDs;

    private bool triggered = false;

    // Start is called before the first frame update
    void Start()
    {
        Observable.Interval(TimeSpan.FromMilliseconds(1000))
            .Subscribe(_ => RandomLoadSound());
    }

    void RandomLoadSound()
    {
        if (!triggered)
        {
            bool triggerSound = Random.Range(0, 100) <= chancesRate;
            if (triggerSound)
            {
                AudioPlayer audioPlayer = FileAPIMgr.OpenAudio(FileAPIMgr.GetMapping(soundIDs[Random.Range(0, soundIDs.Count)]));
                audioPlayer.GetComponent<Window>().onClose.Subscribe(_ => triggered = false);
                triggered = true;
            }
        }
    }

    private void FixedUpdate()
    {
    }
}