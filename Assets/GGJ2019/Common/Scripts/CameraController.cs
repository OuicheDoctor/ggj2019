﻿namespace Windhome
{
    using UnityEngine;

    public enum CRTScanLinesSizes { S32 = 32, S64 = 64, S128 = 128, S256 = 256, S512 = 512, S1024 = 1024 };

    [ExecuteInEditMode]
    public class CameraController : MonoBehaviour
    {
        public Shader _crtShader;
        public float _distortion = 0.1f;
        public float _gamma = 1.0f;
        public float _yExtra = 0.5f;
        public float _curvatureSet1 = 0.5f;
        public float _curvatureSet2 = 1.0f;
        public float _dotWeight = 1.0f;
        public CRTScanLinesSizes _scanSize = CRTScanLinesSizes.S512;
        public Color _rgb1 = Color.white;
        public Color _rgb2 = Color.white;


        private Material _currentMaterial;

        Material material
        {
            get
            {
                if (_currentMaterial == null)
                {
                    _currentMaterial = new Material(_crtShader);
                    _currentMaterial.hideFlags = HideFlags.HideAndDontSave;
                }
                return _currentMaterial;
            }
        }

        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
            material.SetFloat("_Distortion", _distortion);
            material.SetFloat("_Gamma", _gamma);
            material.SetFloat("_curvatureSet1", _curvatureSet1);
            material.SetFloat("_curvatureSet2", _curvatureSet2);
            material.SetFloat("_YExtra", _yExtra);
            material.SetFloat("_rgb1R", _rgb1.r);
            material.SetFloat("_rgb1G", _rgb1.g);
            material.SetFloat("_rgb1B", _rgb1.b);
            material.SetFloat("_rgb2R", _rgb2.r);
            material.SetFloat("_rgb2G", _rgb2.g);
            material.SetFloat("_rgb2B", _rgb2.b);
            material.SetFloat("_dotWeight", _dotWeight);
            material.SetVector("_TextureSize", new Vector2((float)_scanSize, (float)_scanSize));
            Graphics.Blit(source, destination, material);
        }

        void OnDisable()
        {
            if (_currentMaterial)
            {
                DestroyImmediate(_currentMaterial);
            }

        }
    }
}

