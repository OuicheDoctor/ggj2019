﻿namespace Windhome
{
    using UnityEngine;
    using UniRx;

    public class Cursor : MonoBehaviour
    {
        public bool Haunted;
        public Vector2 ForcedPosition;
        public float HauntFactor;
        public Subject<Vector3> onMove = new Subject<Vector3>();
        
        private Vector2 _formerPosition;

        void Start()
        {
            UnityEngine.Cursor.visible = false;
            Observable.EveryUpdate()
                .Subscribe(_ => CursorPosition())
                .AddTo(this);
        }

        private void CursorPosition()
        {
            UnityEngine.Cursor.visible = false;
            _formerPosition = transform.position;
            var mousePosition = (Vector2)Input.mousePosition;
            if (transform.position != Input.mousePosition)
            {
                onMove.OnNext(Input.mousePosition);
            }
            
            if(Haunted)
            {
                var hauntedDirection = ForcedPosition - (Vector2)_formerPosition;
                var playerDirection = mousePosition - _formerPosition;
                hauntedDirection.Normalize();
                playerDirection.Normalize();
                transform.position = _formerPosition + hauntedDirection * HauntFactor + playerDirection;
            }
            else
            {
                transform.position = Input.mousePosition;   
            }
        }
    }
}
