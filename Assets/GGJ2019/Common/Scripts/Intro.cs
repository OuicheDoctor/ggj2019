﻿namespace Windhome
{
    using UnityEngine;
    using UnityEngine.SceneManagement;
    using UnityEngine.Video;

    public class Intro : MonoBehaviour
    {
        public VideoPlayer player;

        private void Start()
        {
            player.loopPointReached += Player_loopPointReached;
        }

        private void Player_loopPointReached(VideoPlayer source)
        {
            SceneManager.LoadScene("Main");
        }
    }
}