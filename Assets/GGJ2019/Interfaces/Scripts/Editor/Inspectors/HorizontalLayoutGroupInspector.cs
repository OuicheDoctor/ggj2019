﻿namespace Windhome.Editor
{
    using UnityEngine.UI;
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(HorizontalLayoutGroup))]
    public class HorizontalLayoutGroupInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if(GUILayout.Button("Recalculate"))
            {
                LayoutRebuilder.ForceRebuildLayoutImmediate(
                    (target as HorizontalLayoutGroup).GetComponent<RectTransform>());
            }
        }
    }
}