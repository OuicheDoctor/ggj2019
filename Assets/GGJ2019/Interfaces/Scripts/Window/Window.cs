﻿namespace Windhome.Interfaces
{
    using System;
    using UniRx;
    using UniRx.Triggers;
    using UnityEngine;
    using UnityEngine.UI;

    public class Window : MonoBehaviour
    {
        public Image _dragAndDropListener;
        public Image _resizeHandle;
        public Button _closeWindowButton;
        
        [HideInInspector]
        public Subject<bool> onClose = new Subject<bool>();
        
        public void SetFocusControl()
        {
            var allImages = GetComponentsInChildren<Image>();
            Array.ForEach(allImages, img =>
            {
                img.OnPointerClickAsObservable()
                .Subscribe(_ =>
                {
                    gameObject.transform.SetAsLastSibling();
                })
                .AddTo(img);
            });
            var allText = GetComponentsInChildren<Text>();
            Array.ForEach(allText, text =>
            {
                text.OnPointerClickAsObservable()
                .Subscribe(_ =>
                {
                    gameObject.transform.SetAsLastSibling();
                })
                .AddTo(text);
            });
        }

        private void Start()
        {
            _dragAndDropListener.OnDragAsObservable()
                .Subscribe(eventData =>
                {
                    transform.position += (Vector3)eventData.delta;
                })
                .AddTo(this);

            _closeWindowButton.OnPointerClickAsObservable()
                .Subscribe(_ =>
                {
                    Destroy(gameObject);
                    onClose.OnNext(true);
                })
                .AddTo(this);

            _resizeHandle.OnPointerClickAsObservable()
                .Subscribe(eventData =>
                {
                    Debug.Log("Click");
                })
                .AddTo(this);

            _resizeHandle.OnDragAsObservable()
                .Subscribe(eventData =>
                {
                    var delta = eventData.delta;
                    delta.y *= -1;
                    (transform as RectTransform).sizeDelta += delta;
                })
                .AddTo(this);
        }

    }
}