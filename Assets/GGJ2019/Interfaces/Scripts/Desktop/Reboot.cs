﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Reboot : MonoBehaviour
{
    private bool rebooted = false;

    void Update()
    {
        if(Input.anyKey)
        {
            if (!rebooted)
            {
                rebooted = true;
                SceneManager.LoadScene("Intro");
            }
        }
    }
}
