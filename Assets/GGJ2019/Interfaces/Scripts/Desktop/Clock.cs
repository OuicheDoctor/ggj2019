﻿namespace Windhome.Interfaces
{
    using System;
    using TMPro;
    using UnityEngine;

    public class Clock : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI _time;

        private void Update()
        {
            _time.text = DateTime.Now.ToLocalTime().ToShortTimeString();
        }
    }
}