using System;
using UniRx;
using UnityEngine.SocialPlatforms;

namespace GGJ2019.Interfaces.Scripts.Desktop
{
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class Background : MonoBehaviour
    {
        public List<Sprite> wallpapers;

        public Image image;

        public float changeChance = .2f;

        private static Background _instance;

        private void Start()
        {
            _instance = this;
        }

        public static void RandomBackground()
        {
            var current = _instance.image.sprite;
            var sprites = new List<Sprite>(_instance.wallpapers);
            sprites.Remove(current);

            _instance.image.sprite = sprites[Random.Range(0, sprites.Count)];
        }

        private void Update()
        {
            if(Input.GetMouseButtonDown(0) && Random.Range(0f, 1f) < changeChance)
            {
                Background.RandomBackground();
            }
        }
    }
}