﻿namespace Windhome
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using UniRx;
    using UniRx.Triggers;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;
    using UnityEngine.Video;
    using Windhome.FileAPI;

    public class Desktop : MonoBehaviour
    {
        public static Desktop desktop;

        public Image RecycleBin;
        public Image CDrive;
        public Image Images;
        public Image IE;
        public Image ClickME;
        public Text ClickMEName;

        public int CDriveID;
        public int BinID;
        public int ImagesID;
        public int JournalID;

        public AudioSource clickSource;

        public float timeToScreenSaver = 30f;
        public GameObject screenSaver; 

        private float lastClick;

        private void Awake()
        {
            desktop = this;
        }

        private IObservable<IList<PointerEventData>> DoubleClickStream(Image source)
        {
            var clickStream = source.OnPointerClickAsObservable();
            return clickStream.Buffer(clickStream.Throttle(TimeSpan.FromMilliseconds(250)))
                .Where(xs => xs.Count >= 2);
        }

        private void Update()
        {
            if(Input.GetMouseButtonDown(0))
            {
                clickSource.Play();
                lastClick = Time.realtimeSinceStartup;
                screenSaver.SetActive(false);
            }

            if(Time.realtimeSinceStartup - lastClick > timeToScreenSaver)
            {
                screenSaver.SetActive(true);
                screenSaver.GetComponent<VideoPlayer>().Play();
            }
        }

        private void Start()
        {
            DoubleClickStream(CDrive)
                .Subscribe(_ =>
                {
                    FileAPI.FileAPIMgr.OpenFolder(FileAPIMgr.GetMapping(CDriveID));
                })
                .AddTo(this);

            DoubleClickStream(RecycleBin)
                .Subscribe(_ =>
                {
                    FileAPI.FileAPIMgr.OpenFolder(FileAPIMgr.GetMapping(BinID));
                })
                .AddTo(this);

            DoubleClickStream(Images)
                .Subscribe(_ =>
                {
                    FileAPI.FileAPIMgr.OpenFolder(FileAPIMgr.GetMapping(ImagesID));
                })
                .AddTo(this);

            DoubleClickStream(IE)
                .Subscribe(_ =>
                {
                    FileAPI.FileAPIMgr.OpenIE();
                })
                .AddTo(this);

            DoubleClickStream(ClickME)
                .Subscribe(_ =>
                {
                    FileAPI.FileAPIMgr.OpenImage(FileAPIMgr.GetMapping(JournalID), _[0].position);
                    ClickMEName.text = "It's your fault.html";
                })
                .AddTo(this);

            FileAPI.FileAPIMgr.OpenTaskManager();
        }
    }
}