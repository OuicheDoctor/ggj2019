﻿using Windhome.FileAPI;

namespace Windhome.Interfaces
{
    using System;
    using UniRx;
    using UniRx.Triggers;
    using UnityEngine;
    using UnityEngine.UI;

    public class WidgetIcon : MonoBehaviour
    {
        public Image icon;
        public string name;

        // Start is called before the first frame update
        void Start()
        {
            var clickStream = icon.OnPointerClickAsObservable();
            clickStream
                .Buffer(clickStream.Throttle(TimeSpan.FromMilliseconds(250)))
                .Where(xs => xs.Count >= 2)
                .Subscribe(_ =>
                {
                    //switch (name)
                    //{
                    //    case "ClickMe":
                    //        FileAPIMgr.OpenImage(name, "Journal");
                    //        GetComponentInChildren<Text>().text = "ItsYourFault.html";
                    //        break;
                        
                    //    case "IE":
                    //        break;
                        
                    //    case "Images":
                    //        break;
                        
                    //    case "DisqueC":
                    //        break;
                        
                    //    case "RecycleBin":
                    //        break;
                    //}
                    //Debug.Log("Open file");
                })
                .AddTo(this);
        }
    }
}
